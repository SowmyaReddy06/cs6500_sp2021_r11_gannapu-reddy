
income = LOAD './DEC_00_SF3_P077_with_ann.csv' USING PigStorage (',') 
as
(ID:chararray, Zip:chararray,  gid1:chararray, gid2:chararray, sal:float);

incomes = FOREACH income GENERATE Zip, sal;

income_group_all = order incomes by sal Asc;

top_10 = LIMIT income_group_all 10;

STORE top_10 INTO './Pig_Query_03.out' using PigStorage(',');