income = LOAD './DEC_00_SF3_P077_with_ann.csv' USING PigStorage (',') 
as
(GEOID:chararray, zipcode:chararray, Geogh1:chararray, Geogh2:chararray, FamilyIncome:float);

incomes = FOREACH income generate zipcode, FamilyIncome;

income_less = FILTER incomes BY FamilyIncome < 50000;

STORE income_less INTO './PigOut01' using PigStorage(',');