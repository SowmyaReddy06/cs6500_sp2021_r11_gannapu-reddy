income = LOAD './DEC_00_SF3_P077_with_ann.csv' USING PigStorage (',') 
as
(Id:chararray, Zipcode:chararray, Geogh1:chararray, Geogh2:chararray, Income:float);

income = FOREACH income GENERATE Id, Zipcode, Geogh1, Geogh2, Income;

group_data = GROUP income ALL;

average_values = FOREACH group_data GENERATE AVG(income.Income) AS avg_income;

filter_data = FILTER income BY Income > average_values.avg_income;

income_asc = order filter_data by Income;

for_each = FOREACH income_asc generate Id, Zipcode, Income;

STORE for_each INTO './Pig_Query_02.out' using PigStorage(',');